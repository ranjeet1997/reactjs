import Footer from "./Layouts/Footer.tsx";
import Header from "./Layouts/Header.tsx";
import Home from "./Pages/Home.tsx";
import {Route, Routes,BrowserRouter} from "react-router-dom" 
function App() {
  return (
    <>
    <Header />
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home />} />
      </Routes>
    </BrowserRouter>

    <Footer />
    </>
  );
}

export default App;
